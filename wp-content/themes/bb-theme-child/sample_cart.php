<?php
/*
Template Name: Sample Cart
*/

get_header(); 
?>

<div class="fl-content-full container">
   <div class="row">
       <div class="fl-content col-md-12">
           
           <div id="shopping-cart">

          </div>

       </div>
   </div>
</div>

<script>
        jQuery( document ).ready(function() {
           // alert( "ready!" );
            get_cart_data();
        });
</script>

<?php get_footer(); ?>